import { Component, OnInit, OnDestroy } from '@angular/core';
import { Comment } from 'src/app/models/comment';
import { PostService } from 'src/app/services/post.service';
import { CommentService } from 'src/app/services/comment.service';
import { forkJoin, Subscription, Observable } from 'rxjs';
import { map, concat, catchError, flatMap } from 'rxjs/operators';
import { Post } from 'src/app/models/post';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { post } from 'selenium-webdriver/http';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit, OnDestroy {

  comments: Array<Comment>;
  post: Post;
  user: User;
  loading: boolean;
  error: boolean = false;
  showComments: boolean = false;
  dataSubscription: Subscription;

  constructor(
    private postService: PostService,
    private commentService: CommentService,
    private userService: UserService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params['id'] != 0 && Number(params['id'] !== NaN)) {
        this.dataSubscription = this.getPostAndComments(params['id']).subscribe((user) => {
          this.user = user;
          this.loading = false;
        });;
      } else {
        this.error = true;
      }
    });
  }

  ngOnDestroy() {
    if (this.dataSubscription) { this.dataSubscription.unsubscribe(); };
  }

  getPostAndComments(id: number): Observable<any> {
    this.loading = true;
    return forkJoin(
      this.postService.getPostById(id),
      this.commentService.getCommentsByPost(id)
    ).pipe(
      catchError(() => {
        this.error = true;
        this.loading = false;
        return []
      }),
      flatMap((result: any) => {
        this.post = result[0];
        this.comments = result[1];
        return this.userService.getUserById(result[0].userId);
      })    
    );
  }

  toggleComments(){
    this.showComments = !this.showComments;
  }

  hideError() {
    this.error = false;
  }

}
