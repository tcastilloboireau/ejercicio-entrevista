import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  @Input()
  posts: Array<Post>;
  @Input()
  users: Array<User>;
  @Output()
  idDelete: EventEmitter<number> = new EventEmitter();

  constructor(private router: Router) { }

  ngOnInit() {
  }

  getUsername(id) {
    const user = this.users.filter(user => user.id === id);
    return user[0].username;
  }

  postDetails(id) {    
    this.router.navigate(['/post', id]);
  }

  deletePost(id) {
    this.idDelete.emit(id);
  }

}
