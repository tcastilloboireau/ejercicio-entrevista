import { Component, OnInit, Output, EventEmitter, Input, Inject } from '@angular/core';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {

  post: Post = null;
  @Output()
  submitPost: EventEmitter<Post> = new EventEmitter<Post>();
  @Input()
  users: Array<User> = [];
  postForm: FormGroup;

  constructor(
    @Inject(FormBuilder) fb: FormBuilder
  ) {
    this.postForm = fb.group({
      user: new FormControl('', [Validators.required]),
      title: new FormControl('', [Validators.required]),
      body: new FormControl('', [Validators.required]),      
    });
  }

  ngOnInit() {
  }

  createPost() {
    this.post = {
      id: null,
      body: null,
      title: null,
      userId: null
    }
  }

  resetForm() {
    this.postForm.reset();
  }

  sendPost() {
    this.post.userId = Number(this.postForm.get('user').value);
    this.post.title = this.postForm.get('title').value;
    this.post.body = this.postForm.get('body').value;    
    this.submitPost.emit(this.post);
    this.resetForm();
    this.post = null;
  }

}
