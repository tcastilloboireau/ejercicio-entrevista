import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-post-filter',
  templateUrl: './post-filter.component.html',
  styleUrls: ['./post-filter.component.css']
})
export class PostFilterComponent implements OnInit {

  @Input()
  users: Array<User>;
  @Output()
  idFilter: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  filterByUser(id) {
    this.idFilter.emit(Number(id));
  }

}
