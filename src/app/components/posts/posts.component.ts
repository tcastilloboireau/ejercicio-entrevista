import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import { Post } from 'src/app/models/post';
import { User } from 'src/app/models/user';
import { forkJoin, pipe, Subscription, Observable } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit, OnDestroy {

  postList: Array<Post> = [];
  filteredPostList: Array<Post> = [];
  userList: Array<User> = [];
  values;
  loading: boolean = false;
  filtered: boolean = false;
  error: boolean = false;
  dataSubscription: Subscription;

  constructor(private postService: PostService, private userService: UserService) { }

  ngOnInit() {
    this.getUsersAndPosts();
    this.dataSubscription = this.getUsersAndPosts().subscribe(() => this.loading = false);
  }

  ngOnDestroy() {
    if(this.dataSubscription) { this.dataSubscription.unsubscribe(); }
  }

  getUsersAndPosts(): Observable<any> {
    this.loading = true;
    return forkJoin(
      this.postService.getPosts(),
      this.userService.getUsers()
    ).pipe(
      catchError(() => {
        this.error = true;
        this.loading = false;
        return []
      }),
      map((result: any) => {
        this.postList = result[0];
        this.filteredPostList = result[0];
        this.userList = result[1];
      })      
    );
  }

  deletePost(id) {
    this.loading = true;
    this.postService.deletePost(id).pipe(
      catchError(() => {
        this.error = true;       
        this.loading = false; 
        return []
      }),
    ).subscribe(() => {
      this.filteredPostList = this.filteredPostList.filter(post => post.id !== id);
      this.postList = this.postList.filter(post => post.id !== id);
      this.loading = false;
    });
  }

  createPost(post) {
    this.loading = true;
    this.postService.createPost(post).pipe(
      catchError(() => {
        this.error = true;       
        this.loading = false; 
        return []
      }),
    ).subscribe((post: Post) => {
      this.filteredPostList = [...this.filteredPostList, post];
      this.postList = [...this.postList, post];
      this.loading = false;
    })
  }

  filterPosts(id) {
    this.filtered = true;
    id === 0 ? this.filteredPostList = this.postList : this.filteredPostList = this.postList.filter(post => post.userId === id);
  }

  hideError() {
    this.error = false;
  }

}
