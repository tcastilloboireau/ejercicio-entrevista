import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.css']
})
export class ErrorMessageComponent implements OnInit {

  @Output()
  closeError: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  dismiss() {
    this.closeError.emit(true);
  }

}
