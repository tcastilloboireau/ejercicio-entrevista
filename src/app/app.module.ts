import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { PostsComponent } from './components/posts/posts.component';
import { AppRoutes } from './app.routes';
import { TableComponent } from './components/posts/table/table.component';
import { AddPostComponent } from './components/posts/add-post/add-post.component';
import { PostDetailComponent } from './components/post-detail/post-detail.component';
import { PostFilterComponent } from './components/posts/post-filter/post-filter.component';
import { PostCommentsComponent } from './components/post-detail/post-comments/post-comments.component';
import { ErrorMessageComponent } from './shared/error-message/error-message.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PostsComponent,
    TableComponent,
    AddPostComponent,
    PostDetailComponent,
    PostFilterComponent,
    PostCommentsComponent,
    ErrorMessageComponent
  ],
  imports: [
    HttpClientModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
