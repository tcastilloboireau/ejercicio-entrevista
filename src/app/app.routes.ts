import { RouterModule, Routes } from '@angular/router';
import { PostsComponent } from './components/posts/posts.component';
import { PostDetailComponent } from './components/post-detail/post-detail.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'posts' },
  { path: 'posts', component: PostsComponent },  
  { path: 'post/:id', component: PostDetailComponent },
];

export const AppRoutes = RouterModule.forRoot(routes, { useHash: true });