import { Geolocation } from './geolocation';

export interface Address {
  street: String;
  suite: String;
  city: String;
  zipCode: String;
  geo: Geolocation;
}
