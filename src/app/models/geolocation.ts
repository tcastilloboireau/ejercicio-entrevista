export interface Geolocation {
  lat: String;
  lng: String;
}