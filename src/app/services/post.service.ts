import { Injectable } from '@angular/core';
import { Post } from '../models/post';

import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  
  constructor(private http: HttpClient) { }

  getPosts(){
    return this.http.get(`${environment.BASE_URL}/posts`);    
  }

  getPostById(id: number){
    return this.http.get(`${environment.BASE_URL}/posts/${id}`);    
  }

  deletePost(id: number){
    return this.http.delete(`${environment.BASE_URL}/posts/${id}`);
  }
  
  createPost(post: Post){
    return this.http.post(`${environment.BASE_URL}/posts`, post);
  }
}
