import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  getCommentsByPost(postId): Observable<any> {
    return this.http.get(`${environment.BASE_URL}/comments?postId=${postId}`);    
  }
}
